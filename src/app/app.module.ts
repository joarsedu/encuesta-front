import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppComponent } from './app.component';
import { IndicatorsModule } from './shared/indicators';
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { NotificationModule } from "./services";
import { MatSidenavModule } from "@angular/material/sidenav";
import { AppRoutingModule } from "@app/app-routing.module";

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    IndicatorsModule,
    BrowserAnimationsModule,
    NotificationModule.forRoot(),
    MatSidenavModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
