import { Component, OnInit } from '@angular/core';
// import { environmet } from "@src/environments/environment.prod";
// import { test } from "@app/test";
import { NotificationService } from "@app/services";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  showSpinner = false;
  title = 'encuesta-front';

  constructor(
    private notification: NotificationService
  ) {}

  ngOnInit(): void {

  }

  onToggleSpinner(): void {
    this.showSpinner = !this.showSpinner;
  }

  onSuccess(): void {
    this.notification.success('Exito');
  }
  onError(): void {
    this.notification.error('Error');
  }

}
